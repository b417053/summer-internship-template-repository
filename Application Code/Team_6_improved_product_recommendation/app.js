var express = require("express"),
    app = express(),
    mysql = require("mysql"),
    bodyParser = require("body-parser");
var expressValidator = require("express-validator"),

    bcrypt = require("bcrypt");
var session = require("express-session");
const saltRounds = 10;
var passport = require("passport");
var { PythonShell } = require('python-shell')
var LocalStrategy = require('passport-local').Strategy;
var MySQLStore = require('express-mysql-session')(session);
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//app.use(expressValidator());

var user = { auth: false, id: null }, log = null;
app.use(passport.initialize());
app.use(passport.session());
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "demodell"
});
app.use(session({
    secret: "hakuna matata",
    resave: false,
    saveUninitialized: false,
    //  store: sessionStore

}));

// con.connect(function (err) {
//     if (err) throw err;
//     console.log("Connected!");
//     var sql = "CREATE TABLE users (id INT AUTO_INCREMENT PRIMARY KEY,username VARCHAR(50), name VARCHAR(50),password VARCHAR(50))";
//     con.query(sql, function (err, result) {
//         if (err) {
//             console.log(err);
//         } else {
//             console.log("Table created");
//         }
//     });
// });
var bool = null;
function check(username, password, req, res) {
    con.query("select password,id from users where username=? ", [username], function (err, resl, field) {

        if (resl.length === 0) {
            bool = false;
            res.redirect("/login")
        }
        if (password == resl[0].password) {
            user.auth = true;
            user.id = resl[0].id;
            res.redirect("/dell")
        }

    })
}
app.get("/home", function (req, res) {


    PythonShell.run('C:/Users/user/Documents/WebDevelopment/test/demo_dell/demodell.py', null, function (err) {
        if (err) {
            console.log(err);
        }
        console.log('finished');
        res.render("home.ejs");
    });
    res.render("home.ejs");
})


app.get("/dell/viewall", is, function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where labels=(select label from result where id=1)", function (err, resu) {

            res.render("dellshow.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/dell", is, function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where labels=(select label from result where id=1)", function (err, resu) {

            res.render("dell.ejs", { name: result[0].username, lab: resu });
        })
    })

});
app.get("/find", is, function (req, res) {

    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from store where city=(select city from users where username=?)", result[0].username, function (err, resu) {
            console.log(resu);
            res.render("store.ejs", { name: result[0].username, lap: resu });
        })
    })
})
app.get("/dell/rec/:id", is, function (req, res) {
    var name = req.params.id;
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where laptop_name=?", [name], function (err, resu) {
            console.log(resu);
            res.render("show.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/dell/student", is, function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where category='student'", function (err, result1) {
            console.log(result1);
            res.render("dell_student.ejs", { name: result[0].username, lap: result1 });
        })

    })
})

app.get("/dell/pro", is, function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where category='professional'", function (err, result1) {
            console.log(result1);
            res.render("dell_pro.ejs", { name: result[0].username, lap: result1 });
        })

    })
})

app.get("/dell/gaming", is, function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where category='gaming'", function (err, result1) {
            console.log(result1);
            res.render("dell_gaming.ejs", { name: result[0].username, lap: result1 });
        })

    })
})
app.get("/dell/home", is, function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where category='home user'", function (err, result1) {
            console.log(result1);
            res.render("dell_home.ejs", { name: result[0].username, lap: result1 });
        })

    })
})
app.get("/dell/programmer", is, function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where category='programmer'", function (err, result1) {
            console.log(result1);
            res.render("dell_prog.ejs", { name: result[0].username, lap: result1 });
        })

    })
})
app.get("/dell/student/:id", function (req, res) {
    var name = req.params.id;
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where laptop_name=?", [name], function (err, resu) {
            console.log(resu);
            res.render("show.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/dell/pro/:id", function (req, res) {
    var name = req.params.id;
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where laptop_name=?", [name], function (err, resu) {
            console.log(resu);
            res.render("show.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/dell/game/:id", function (req, res) {
    var name = req.params.id;
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where laptop_name=?", [name], function (err, resu) {
            console.log(resu);
            res.render("show.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/dell/home/:id", function (req, res) {
    var name = req.params.id;
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where laptop_name=?", [name], function (err, resu) {
            console.log(resu);
            res.render("show.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/dell/prog/:id", function (req, res) {
    var name = req.params.id;
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where laptop_name=?", [name], function (err, resu) {
            console.log(resu);
            res.render("show.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/dell/rec/:id", function (req, res) {
    var name = req.params.id;
    con.query("select username from users where id=?", [user.id], function (err, result) {
        con.query("select * from data where laptop_name=?", [name], function (err, resu) {
            console.log(resu);
            res.render("show.ejs", { name: result[0].username, lab: resu });
        })
    })
})
app.get("/signup", function (req, res) {

    res.render("register.ejs", { err: null });
})
app.post("/login", function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var i = check(username, password, req, res);

})
app.post("/signup", function (req, res) {
    var username = req.body.username;
    var name = req.body.name;
    var password = req.body.password;
    var age = req.body.age;
    var city = req.body.city;
    con.query("select username from users where username=?", username, function (err, res) {
        if (res.length > 1) {
            res.render("register.ejs", { err: err });
        }
    })
    con.query("Insert into users(username,name,password,age,city) values (?,?,?,?,?)", [username, name, password, age, city], function (error, results, fields) {
        if (error) {
            res.render("register.ejs", { err: error });
        } else {
            con.query("select id from users where username=?", username, function (err, res) {
                if (err) throw err;
                const id = res[0];
                console.log(id);
                req.login(id, function (err) {

                    console.log("logged in");

                })
                console.log(req.user);
                user = { auth: true, id: req.user };

            })

            res.render("login.ejs", { log: true });
        }

    })
})
app.get("/del", function (req, res) {
    con.query("select username from users where id=?", [user.id], function (err, result) {
        res.redirect("/dell");
    })
})
app.get("/logout", is, function (req, res) {
    user.auth = false;
    user.id = null;
    res.redirect("/home");
})
function is(req, res, next) {
    if (user.auth) {
        next();
    } else {
        res.redirect("/login");
    }
}
passport.serializeUser(function (id, done) {
    done(null, id);
});

passport.deserializeUser(function (id, done) {

    done(err, id);

});
app.get("/login", function (req, res) {

    res.render("login.ejs", { log: null });


})
app.get("/amakart", function (req, res) {
    res.render("amakart.ejs");
})
app.listen("3000", function () {
    console.log("Server has started");
});
