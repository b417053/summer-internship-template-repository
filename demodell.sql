-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2019 at 07:31 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demodell`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `labels` int(11) DEFAULT NULL,
  `laptop_name` varchar(40) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `processorspeed` varchar(7) DEFAULT NULL,
  `ram_capacity` varchar(4) DEFAULT NULL,
  `ram_type` varchar(5) DEFAULT NULL,
  `harddrive` varchar(6) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `screen_size` float DEFAULT NULL,
  `warranty` int(11) DEFAULT NULL,
  `gpu` varchar(28) DEFAULT NULL,
  `os` varchar(10) DEFAULT NULL,
  `userratings` float DEFAULT NULL,
  `category` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`labels`, `laptop_name`, `price`, `processorspeed`, `ram_capacity`, `ram_type`, `harddrive`, `weight`, `screen_size`, `warranty`, `gpu`, `os`, `userratings`, `category`) VALUES
(1, 'DELL NEW VOSTRO 15 3590', 43958, '4.2 Ghz', '4GB', 'DDR4', '1 TB', 1.99, 39.6, 1, 'Intel Iris Plus Graphics 640', 'Windows 10', 3.5, 'professional'),
(1, 'DELL Vostro 14 5000 Laptop', 52000, '3.9 Ghz', '4GB', 'DDR4', '2 TB', 1.55, 35.5, 1, 'Intel HD Graphics 6000', 'Windows 10', 3.5, 'professional'),
(1, 'DELL New Vostro 14 3480 Laptop', 26000, '3.5 Ghz', '4GB', 'DDR5', '3 TB', 1.46, 31.5, 1, 'Intel HD Graphics 6050', 'Linux', 3.5, 'professional'),
(1, 'DELL Vostro 14 5490 Business Laptop', 45000, '4.2 Ghz', '4GB', 'DDR6', '4 TB', 1.49, 36.5, 1, 'Intel Iris Plus Graphics 650', 'No OS', 3.5, 'professional'),
(1, 'DELL Latitude 3400 Business Laptop', 41000, '3.9 Ghz', '4GB', 'DDR7', '5 TB', 1.98, 39.6, 1, 'Intel HD Graphics 6000', 'No OS', 3.5, 'professional'),
(1, 'DELL Latitude 5300 2-in-1', 82000, '1.6 Ghz', '4GB', 'DDR8', '6 TB', 1.43, 33.02, 1, 'Intel HD Graphics 615', 'Windows 10', 3.5, 'professional'),
(1, 'DEll Latitude 5400 Chromebook Enterprise', 65000, '4.2 Ghz', '4GB', 'DDR9', '7 TB', 1.5, 35.6, 1, 'AMD Radeon R5 M430', 'No OS', 3.5, 'professional'),
(1, 'DEll Latitude 7200 2-in-1 Laptop', 102558, '1.6Ghz', '4GB', 'DDR10', '8 TB', 0.9, 31.2, 1, 'Intel UHD Graphics 620', 'No OS', 3.5, 'professional'),
(2, 'DELL G3 Series', 59990, '4Ghz', '8GB', 'DDR4', '1 TB', 2.53, 39.62, 1, 'Nvidia GeForce GTX 1050', 'Linux', 4, 'gaming'),
(2, 'DELL G7 Series', 123999, '4.1Ghz', '16GB', 'DDR4', '1 TB', 2.63, 39.62, 1, 'AMD Radeon 530', 'Linux', 4, 'gaming'),
(2, 'DELL Inspiron 3000', 92990, '4.5Ghz', '8GB', 'DDR5', '1 TB', 2.5, 39.62, 1, 'Nvidia GeForce 940MX', 'Linux', 4, 'gaming'),
(2, 'DELL G3 15 3000', 65950, '4Ghz', '8GB', 'DDR4', '2 TB', 2.53, 39.62, 1, 'Nvidia GeForce GTX 1050', 'Windows 10', 4.5, 'gaming'),
(2, 'DELL NEW ALIENWARE M15', 160690, '4.5Ghz', '8GB', 'DDR4', '512 GB', 2.16, 39.62, 1, 'Nvidia GeForce 940MX', 'Windows 10', 3.5, 'gaming'),
(2, 'DELL Inspiron', 104697, '3.8Ghz', '16GB', 'DDR4', '1 TB', 2.62, 39.62, 2, 'Nvidia GeForce GTX 1050 Ti', 'Windows 10', 4, 'gaming'),
(2, 'DELL G3 3579 Gaming Laptop', 114900, '2.2Ghz', '16GB', 'DDR4', '1 TB', 2.5, 39.62, 2, 'AMD Radeon R5', 'No OS', 4, 'gaming'),
(2, 'DELL Inspiron 7559 Notebook', 57907, '3.2Ghz', '8GB', 'DDR3', '1 TB', 2.57, 39.62, 1, 'Nvidia GeForce GTX 1050', 'Linux', 4, 'gaming'),
(3, 'DELL XPS 13 9370', 119990, '3.2Ghz', '8GB', 'DDR4', '1 TB', 2, 33.78, 1, 'Nvidia GeForce 150MX', 'Linux', 4, 'student'),
(3, 'DELL Inspiron CHROMEBOOK', 38740, '1.6Ghz', '4GB', 'DDR3', '1 TB', 1.1, 29.464, 1, 'Intel Iris Plus Graphics 640', 'Linux', 3, 'student'),
(3, 'DELL Vostro 3840', 27990, '3.9Ghz', '4GB', 'DDR4', '1 TB', 1.7, 35.56, 1, 'AMD R4 Graphics', 'Windows 10', 3, 'student'),
(3, 'DELL Inspiron 5593', 62490, '1.00Ghz', '8GB', 'DDR4', '1 TB', 1.93, 39.62, 1, 'AMD Radeon R5', 'Windows 10', 3, 'student'),
(3, 'DELL New XPS 15 Laptop', 139790, '4.5Ghz', '16GB', 'DDR4', '512GB', 1.8, 39.6, 1, 'Nvidia GeForce 940MX', 'Windows 10', 3, 'student'),
(3, 'DELL NEW Inspiron 14 5482 2-IN-1', 43890, '3.9Ghz', '8GB', 'DDR4', '1 TB', 1.7, 35.56, 3, 'Nvidia GeForce GTX 1050 Ti', 'Windows 10', 4.5, 'student'),
(3, 'DELL LATITUDE E5440', 27990, '1.9Ghz', '8GB', 'DDR4', '500GB', 1.9, 35.56, 3, 'Intel UHD Graphics 620', 'Linux', 4.5, 'student'),
(3, 'DELL LATITUDE 3500', 31990, '3.8Ghz', '4GB', 'DDR4', '1024GB', 3.02, 39.62, 3, 'Intel HD Graphics 615', 'no OS', 4.5, 'student'),
(4, 'DELL LATITUDE 3501', 32990, '3.9Ghz', '4GB', 'DDR5', '1024GB', 3.02, 39.62, 3, 'AMD Radeon R5 M430', 'Windows 10', 4.5, 'home user'),
(4, 'DELL LATITUDE 3502', 31890, '3.7Ghz', '4GB', 'DDR6', '1024GB', 3.02, 39.62, 3, 'Intel Iris Plus Graphics 640', 'Windows 10', 4.5, 'home user'),
(4, 'DELL LATITUDE 3503', 31970, '3.65Ghz', '4GB', 'DDR7', '1024GB', 3.02, 39.62, 1, 'Nvidia GeForce MX150', 'No OS', 4.5, 'home user'),
(4, 'DELL LATITUDE 3504', 31990, '1.00Ghz', '4GB', 'DDR8', '1024GB', 3.02, 39.62, 2, 'AMD R4 Graphics', 'No OS', 4.5, 'home user'),
(4, 'DELL LATITUDE 3505', 41990, '3.9Ghz', '4GB', 'DDR9', '1024GB', 3.02, 39.62, 1, 'Intel UHD Graphics 620', 'Linux', 2.5, 'home user'),
(4, 'DELL LATITUDE 3506', 32990, '3.22Ghz', '4GB', 'DDR10', '1024GB', 3.02, 39.62, 1, 'Nvidia GeForce 940MX', 'Linux', 3.5, 'home user'),
(4, 'DELL LATITUDE 3507', 35990, '3.89Ghz', '4GB', 'DDR11', '1024GB', 3.02, 39.62, 1, 'Intel HD Graphics 615', 'No OS', 4, 'home user'),
(4, 'DELL LATITUDE 3508', 31995, '3.3Ghz', '4GB', 'DDR12', '1024GB', 3.02, 39.62, 1, 'AMD Radeon R5', 'No OS', 3.5, 'home user'),
(5, 'DELL LATITUDE 3509', 31790, '3.2Ghz', '4GB', 'DDR13', '1024GB', 3.02, 39.62, 2, 'Nvidia GeForce MX150', 'Windows 10', 2.5, 'programmer'),
(5, 'DELL LATITUDE 3510', 37990, '3.1Ghz', '4GB', 'DDR14', '1024GB', 3.02, 39.62, 2, 'AMD Radeon R5 M430', 'Windows 10', 4.5, 'programmer'),
(5, 'DELL LATITUDE 3511', 36790, '3.67Ghz', '4GB', 'DDR15', '1024GB', 3.02, 39.62, 2, 'Intel UHD Graphics 620', 'Windows 10', 4.5, 'programmer'),
(5, 'DELL LATITUDE 3512', 41790, '2.2Ghz', '8GB', 'DDR16', '1024GB', 3.02, 39.62, 2, 'Nvidia GeForce 150MX', 'Linux', 4.5, 'programmer'),
(5, 'DELL Inspiron 5593', 62495, '4Ghz', '8GB', 'DDR17', '512GB', 2.57, 35.56, 2, 'Nvidia GeForce GTX 1050 Ti', 'Linux', 4.5, 'programmer'),
(5, 'DELL Inspiron 5594', 64490, '4.5Ghz', '8GB', 'DDR18', '512GB', 2.57, 35.56, 2, 'AMD Radeon R5 M430', 'No OS', 4.5, 'programmer'),
(5, 'DELL Inspiron 5595', 62499, '4.2Ghz', '8GB', 'DDR19', '512GB', 2.57, 35.56, 1, 'Intel Iris Plus Graphics 640', 'No OS', 4.5, 'programmer'),
(5, 'DELL Inspiron 5596', 65990, '4.7Ghz', '8GB', 'DDR20', '512GB', 2.57, 35.56, 1, 'Nvidia GeForce 940MX', 'Windows 10', 4.5, 'programmer');

-- --------------------------------------------------------

--
-- Table structure for table `dell`
--

CREATE TABLE `dell` (
  `label` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `ram` int(11) DEFAULT NULL,
  `gpu` varchar(255) DEFAULT NULL,
  `cpu` varchar(255) DEFAULT NULL,
  `screen` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `OS` varchar(255) DEFAULT NULL,
  `cat` varchar(255) DEFAULT NULL,
  `warranty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `label` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `label`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) UNSIGNED NOT NULL,
  `data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `city` varchar(11) DEFAULT NULL,
  `nameofthestore` varchar(21) DEFAULT NULL,
  `userrating` float DEFAULT NULL,
  `contactnumber` int(11) DEFAULT NULL,
  `openclose` varchar(11) DEFAULT NULL,
  `locality` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`city`, `nameofthestore`, `userrating`, `contactnumber`, `openclose`, `locality`) VALUES
('AHMEDABAD', 'DELL EXCLUSIVE STORE', 4, 2147483647, '9AM-9PM', 'Naroda'),
('AHMEDABAD', 'DELL  STORE', 4.6, 2147483647, '9AM-9PM', 'osriudfvh'),
('BANGALORE', 'LAPTOP STORE', 4.4, 2147483647, '11AM-9PM', 'ldkvm'),
('BANGALORE', 'DELL LAPTOP STORE', 4.4, 2147483647, '11AM-9PM', 'ldsldjvnsdfv'),
('CHENNAI', 'LAPTOP FORUM', 3, 2147483647, '11AM-9PM', 'dkvmws[drp'),
('CHENNAI', 'LAPTOP FORUM', 4, 2147483647, '11AM-9PM', 'oiuhgfoeirg'),
(' NEW DELHI', 'DELL EXCLUSIVE STORE', 3.4, 2147483647, '11AM-9PM', 'iuwrhgi'),
('NEW DELHI', 'COMPUTER MART', 4.4, 2147483647, '11AM-9PM', 'lredigfpworejf'),
('HYDERABAD', 'DELL SHOWROOM', 4.6, 2147483647, '11AM-9PM', 'efjwpoekf'),
('HYDERABAD', 'DELL STORE', 4, 2147483647, '11AM-9PM', 'erfvergv'),
('LUCKNOW', 'DELL EXCLUSIVE STORE', 4.2, 2147483647, '9AM-6PM', 'rgvfsrehbt'),
('LUCKNOW', 'SAI LAPTOP TECHNOLOGY', 4.1, 2147483647, '11AM-8PM', 'hjtyukjf'),
('KOLKATA', 'DELL STORE', 3.7, 2147483647, '11AM-8PM', 'rhrth'),
('KOLKATA', 'COMPUTER SHOP', 4, 2147483647, '11AM-8PM', 'rfbhrtj'),
('JAIPUR', 'LAPTOP MART', 3.8, 2147483647, '9AM-6PM', 'frth6yj'),
('JAIPUR', 'DELL STORE', 3.5, 2147483647, '9AM-6PM', 'rhryj'),
('MUMBAI', 'WIZ WORLD', 4.2, 2147483647, '9AM-6PM', 'rhjtryk'),
('MUMBAI', 'DELL STORE', 4, 2147483647, '9AM-6PM', 'aegryk'),
('BHUBANESWAR', 'SPECTRUM TECHNOLOGY', 3.7, 2147483647, '11AM-6:30PM', 'rshty7klo'),
('BHUBANESWAR', 'DELL STORE', 4.3, 2147483647, '11AM-6:30PM', 'rhr6ij');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `city`, `age`) VALUES
(1, 'ash', 'ash', 'ash', 'JAIPUR', 18);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dell`
--
ALTER TABLE `dell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
