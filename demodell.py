import mysql.connector
#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
from IPython.display import display

import sklearn
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
import warnings; warnings.simplefilter('ignore')
import collections
from sklearn.preprocessing import Imputer


dataset = pd.read_csv("data_target.csv",header=None)
# dataset=pd.read_csv("MLData.csv",header=None)
# display(dataset.shape)
# #testing
testset = pd.read_csv("user.csv",header=None)
dataset=pd.concat([dataset,testset])
# dataset.head

# Removing the 0th row
dataset = dataset.iloc[1:,:]
display(dataset.shape)
dataset.head()


# In[4]:


# storing the last column in y excluding the last row
y=dataset.iloc[:-1,-1:]
#print(y.shape)

#storing the data in x
x=dataset.iloc[:,:-1].values
#print(x.shape)


# In[5]:


# display(set(dataset.iloc[:,1]))
#print("Types of city",set(dataset.iloc[:,1]))


# In[ ]:





# In[6]:


# clean data
imputer = Imputer(missing_values='NaN', strategy='mean', axis=0)
imputer.fit(x[:,[0,2,4,5,6,7,8,9,10]])
x[:,[0,2,4,5,6,7,8,9,10]] = imputer.transform(x[:,[0,2,4,5,6,7,8,9,10]])
#print(x)


# In[7]:


#converting string to numbers
labelencoder1=LabelEncoder()

x[:,1]=labelencoder1.fit_transform(x[:,1])
#print(x.shape,y.shape)


# In[8]:


#applying one hot encoder to give equal weights to each type 2 4 5 8
onehotencoder1=OneHotEncoder(categorical_features=[1])
x = onehotencoder1.fit_transform(x).toarray()
x.shape


# In[ ]:





# In[9]:


#training and testing
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x[:-1], y, test_size = 0.20)
#print(x_train.shape)
#print(x_test.shape)
#print(y_train.shape)
#print(y_test.shape)
#print(type(y_train))


# In[10]:


#feature Scaling
from sklearn.preprocessing import StandardScaler
standardscaler_x=StandardScaler()
x_train=standardscaler_x.fit_transform(x_train)
x_test=standardscaler_x.fit_transform(x_test)
#print(x_train.shape)
#print(x_test.shape)


# In[11]:


models = []
models.append(('Logistic Regression', LogisticRegression()))
models.append(('Linear Discriminant Analysis', LinearDiscriminantAnalysis()))
#models.append(('K-Nearest Neighbour ', KNeighborsClassifier()))
models.append(('Decison Tree', DecisionTreeClassifier()))
models.append(('Guassian Naive Bayes', GaussianNB()))
models.append(('SVM', SVC()))


# In[12]:


for name,model in models:
    
    # Fitting Naive Bayes to the Training set
    classifier = model
    classifier.fit(x_train, y_train)

    # Predicting the Test set results
    y_pred = classifier.predict(x_test)
    #print(y_pred)

# display(y_test.transform)
# display(y_pred)


# In[20]:


#testing for the user
x_test=x[-1:]
#print(x_test)
s=[]
for name,model in models:
    
    # Fitting Naive Bayes to the Training set
    classifier = model
    classifier.fit(x_train, y_train)
    
    # Predicting the Test set results
    y_pred = classifier.predict(x_test)
    #print(y_pred)
    s.append(y_pred)
display(y_test.transform)
display(y_pred)


# In[22]:


ans=[]
for a in s:
    ans.append(a[0])
ans=collections.Counter(ans)
ans


# In[23]:


#max label
temp=-1
res=0
for a,b in ans.items():
    if temp<b:
        temp=b
        res=a
#print(res)











mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="",
  database="demodell"
)
mycursor = mydb.cursor()
val=res
sql = "update result set label="+str(val)+" where id=1"
mycursor.execute(sql, val)


mydb.commit()
